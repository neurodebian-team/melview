#!/usr/bin/make -f

# see EXAMPLES in dpkg-buildflags(1) and read /usr/share/dpkg/*
DPKG_EXPORT_BUILDFLAGS = 1
include /usr/share/dpkg/default.mk

srcpkg = $(shell LC_ALL=C dpkg-parsechangelog | grep '^Source:' | cut -d ' ' -f 2,2)
debver = $(shell LC_ALL=C dpkg-parsechangelog | grep '^Version:' | cut -d ' ' -f 2,2 )
upstreamver = $(shell echo $(debver) | cut -d '-' -f 1,1 )

# this figures out the last merge point from 'master' into the Debian branch and
# then described this commit relative to the last release tag (V...)
# If this should make any sense the local upstream branch must track upstream's
# master or whatever other source branch.
gitver = $(shell [ -x /usr/bin/git ] && git describe --tags --match 'v[0-9]\.[0-9]\.[0-9]' $$(git merge-base -a HEAD upstream) | sed -e 's/^v//' -e 's/-/+git/')~dfsg.1

# one ring to rule them all ...
%:
	dh $@ --with python2

clean::
	dh_clean
	-rm -rf melview.egg-info dist build
	-rm -f melview.ini
	-rm -f *.pyc fsl/*.pyc melview/*.pyc

dfsg-source-tree:
	-quilt pop -a
	@echo "Testing for uncommited changes"
	@git diff --quiet HEAD
# remove cruft
	rm -f .DS_Store ._PythonLog .project
# commit any cleanup results
	@if ! git diff --quiet HEAD; then \
		git commit -e -a -m "Remove unwanted (e.g. non-DFSG-compliant) content"; \
	fi

# make orig tarball from repository content
get-orig-source: dfsg-source-tree
	# orig tarball, turn directory into something nicer
	git archive --format=tar --prefix=$(srcpkg)-$(gitver)/ HEAD | \
		gzip -9 > $(srcpkg)_$(gitver).orig.tar.gz

# check that DSC patches still apply
maint-check-dsc-patches:
	@for p in debian/patches/*-dsc-patch; \
		do echo "check $$p"; \
		patch -p1 --dry-run < $$p || exit 1 ; \
	done

